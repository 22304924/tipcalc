//
//  TipCalcApp.swift
//  TipCalc
//
//  Created by Dale on 14/11/2020.
//

import SwiftUI

@main
struct TipCalcApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
